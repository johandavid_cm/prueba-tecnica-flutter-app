import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as storage;
import 'package:prueba_tecnica_app/config/api_config.dart';
import 'package:prueba_tecnica_app/models/empleado_response_model.dart';

class EmpleadoService {
  EmpleadoService._privateConstructor();
  static final EmpleadoService _instance =
      EmpleadoService._privateConstructor();
  factory EmpleadoService() {
    return _instance;
  }
  final _dio = Dio();

  Future<EmpleadoResponse?> getEmpleados({int page = 1}) async {
    try {
      final response = await _dio.get(
        "$API_URL/empleados",
        queryParameters: {'page': page},
        options: Options(
          headers: {
            'Content-type': 'application/json',
            'x-token':
                await storage.FlutterSecureStorage().read(key: 'token') ?? '',
          },
        ),
      );
      final empleadoResponse = EmpleadoResponse.fromJson(response.data);
      final ok = empleadoResponse.ok ?? false;
      if (ok) {
        return empleadoResponse;
      }
      return null;
    } catch (e) {
      if (e is DioError) {
        if (e.response?.data != null) {
          return EmpleadoResponse.fromJson(e.response!.data);
        }
      }
      return null;
    }
  }

  Future<Map<String, dynamic>?> eliminarEmpleado(
      {required int idEmpleado}) async {
    try {
      final response = await _dio.delete(
        "$API_URL/empleados/$idEmpleado",
        options: Options(
          headers: {
            'Content-type': 'application/json',
            'x-token':
                await storage.FlutterSecureStorage().read(key: 'token') ?? '',
          },
        ),
      );
      return response.data;
    } catch (e) {
      if (e is DioError) {
        if (e.response != null) {
          return e.response!.data;
        }
      }
      return null;
    }
  }
}
