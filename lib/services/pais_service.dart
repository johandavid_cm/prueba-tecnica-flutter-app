import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as storage;
import 'package:prueba_tecnica_app/config/api_config.dart';

class PaisService {
  PaisService._privateConstructor();
  static final PaisService _instance = PaisService._privateConstructor();
  factory PaisService() {
    return _instance;
  }
  final _dio = Dio();

  Future<Response<dynamic>?> getPaises() async {
    try {
      final response = await _dio.get(
        "$API_URL/pais",
        options: Options(
          headers: {
            'Content-type': 'application/json',
            'x-token':
                await storage.FlutterSecureStorage().read(key: 'token') ?? '',
          },
        ),
      );
      return response;
    } catch (e) {
      if (e is DioError) {
        return e.response;
      }
      return null;
    }
  }
}
