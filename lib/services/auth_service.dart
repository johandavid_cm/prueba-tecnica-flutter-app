import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as storage;
import 'package:prueba_tecnica_app/config/api_config.dart';

class AuthService {
  AuthService._privateConstructor();
  static final AuthService _instance = AuthService._privateConstructor();
  factory AuthService() {
    return _instance;
  }
  final _dio = Dio();

  Future<Response<dynamic>?> login(Map<String, dynamic> data) async {
    try {
      final response = await _dio.post("$API_URL/auth",
          data: data,
          options: Options(headers: {'Content-type': 'application/json'}));
      return response;
    } catch (e) {
      if (e is DioError) {
        return e.response;
      }
      return null;
    }
  }

  Future<Response<dynamic>?> renewToken() async {
    try {
      final token = await storage.FlutterSecureStorage().read(key: 'token');
      final res = await _dio.get("$API_URL/auth/renew",
          options: Options(headers: {'x-token': token ?? ''}));
      return res;
    } catch (e) {
      if (e is DioError) {
        return e.response;
      } else {
        return null;
      }
    }
  }
}
