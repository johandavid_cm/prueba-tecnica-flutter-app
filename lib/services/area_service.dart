import 'package:dio/dio.dart';
import 'package:prueba_tecnica_app/config/api_config.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as storage;

class AreaService {
  AreaService._privateConstructor();
  static final AreaService _instance = AreaService._privateConstructor();
  factory AreaService() {
    return _instance;
  }
  final _dio = Dio();

  Future<Response<dynamic>?> getAreas() async {
    try {
      final response = await _dio.get(
        "$API_URL/area",
        options: Options(
          headers: {
            'Content-type': 'application/json',
            'x-token':
                await storage.FlutterSecureStorage().read(key: 'token') ?? '',
          },
        ),
      );
      return response;
    } catch (e) {
      if (e is DioError) {
        return e.response;
      }
      return null;
    }
  }
}
