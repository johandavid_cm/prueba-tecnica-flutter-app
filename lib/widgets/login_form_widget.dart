part of 'widgets.dart';

class LoginForm extends StatefulWidget {
  LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController controllerEmail = TextEditingController();
  final TextEditingController controllerPassword = TextEditingController();
  String? errorText;
  bool cargando = false;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Form(
      key: _formKey,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: size.width * 0.07),
        child: Column(
          children: [
            CustomTextField(
              labelText: 'Email',
              controller: controllerEmail,
              validator: validarCorreo,
              keyboardType: TextInputType.emailAddress,
            ),
            SizedBox(
              height: 15.0,
            ),
            CustomTextField(
              labelText: 'Password',
              controller: controllerPassword,
              validator: validarPassword,
              obscureText: true,
            ),
            SizedBox(
              height: 15.0,
            ),
            errorText != null
                ? Text(
                    errorText!,
                    style: TextStyle(color: Colores.danger),
                  )
                : Container(),
            MaterialButton(
              onPressed: realizarLogin,
              minWidth: size.width * 0.5,
              child: !cargando
                  ? Text(
                      'Ingresar',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    )
                  : Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: CircularProgressIndicator(
                        color: Colors.white,
                      ),
                    ),
              color: Colores.primario,
            )
          ],
        ),
      ),
    );
  }

  void realizarLogin() async {
    if (_formKey.currentState!.validate()) {
      setState(() {
        errorText = null;
        cargando = true;
      });
      final data = Map<String, dynamic>();
      data['email'] = controllerEmail.text;
      data['password'] = controllerPassword.text;
      final resp = await AuthService().login(data);

      cargando = false;
      if (resp != null) {
        if (resp.data["ok"]) {
          // Si la respuesta de la api es correcta procedemos a hacer
          // el login en la app y guardar el token en el localstorage
          final usuarioResponse = UsuarioResponse.fromJson(resp.data);
          await FlutterSecureStorage()
              .write(key: 'token', value: usuarioResponse.token);
          BlocProvider.of<UsuarioBloc>(context)
              .add(OnLogin(usuarioResponse.usuario));
          Navigator.pushNamedAndRemoveUntil(context, 'home', (route) => false);
        } else {
          errorText = resp.data['msg'];
        }
      }
      setState(() {});
    }
  }
}
