part of 'widgets.dart';

class EmpleadoWidget extends StatelessWidget {
  final Empleado empleado;
  const EmpleadoWidget({
    Key? key,
    required this.empleado,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(
        key: Key(this.empleado.id.toString()),
      ),
      child: _construirEmpleado(),
      actions: [
        IconSlideAction(
          color: Colores.morado,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => EditEmpleado(
                  empleado: empleado,
                ),
              ),
            );
          },
          caption: 'Editar',
          icon: Icons.edit,
        ),
      ],
      secondaryActions: [
        IconSlideAction(
          color: Colores.danger,
          onTap: () {
            Dialogs.showDialogConfirm(context, onpressed: () async {
              final resp = await EmpleadoService()
                  .eliminarEmpleado(idEmpleado: empleado.id ?? 0);

              Navigator.pop(context);
              if (resp != null) {
                if (resp["ok"]) {
                  BlocProvider.of<EmpleadoBloc>(context)
                      .add(OnEliminarEmpleado(empleado));
                  Dialogs.showDialog(
                    context,
                    child: Text(
                      resp["msg"],
                      textAlign: TextAlign.center,
                    ),
                    title: 'Empleado eliminado correctamente',
                    barrierDismissible: true,
                  );
                } else {
                  Dialogs.showDialog(
                    context,
                    child: Text(
                      resp["msg"],
                      textAlign: TextAlign.center,
                    ),
                    title: 'Error',
                    barrierDismissible: true,
                  );
                }
              }
            });
          },
          caption: 'Eliminar',
          icon: Icons.edit,
        ),
      ],
    );
  }

  Widget _construirEmpleado() {
    return InkWell(
      onTap: () {
        //TODO: Ir a la página del detalle de ingresos y salidas
      },
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Row(
          children: [
            Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Container(
                  padding: EdgeInsets.all(8.0),
                  color: Colores.primario,
                  child: Text(
                    this.empleado.getFirstLetters(),
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20.0,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    this.empleado.getNombreCompleto(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colores.primario,
                      fontSize: 22,
                    ),
                  ),
                  Text(
                    this.empleado.email ?? '',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colores.primario,
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    '${this.empleado.tipoIdentificacion?.codigo ?? ''} ${this.empleado.numeroIdentificacion ?? ''}',
                    style: TextStyle(
                      color: Colores.primario,
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    '${this.empleado.area?.nombre ?? ''}',
                    style: TextStyle(
                      color: Colores.primario,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 50.0,
              child: Image.asset(
                'icons/flags/png/${this.empleado.pais?.codigo ?? 'us'}.png',
                package: 'country_icons',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
