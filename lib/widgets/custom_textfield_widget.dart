part of 'widgets.dart';

class CustomTextField extends StatelessWidget {
  final String labelText;
  final TextEditingController controller;
  final bool obscureText;
  final TextInputType keyboardType;
  final String? Function(String?)? validator;
  const CustomTextField({
    Key? key,
    required this.labelText,
    required this.controller,
    this.obscureText = false,
    this.keyboardType = TextInputType.text,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5.0),
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        color: Colors.blueGrey[50],
        child: TextFormField(
          validator: this.validator,
          obscureText: this.obscureText,
          controller: this.controller,
          keyboardType: keyboardType,
          decoration: InputDecoration(
            labelText: this.labelText,
          ),
        ),
      ),
    );
  }
}
