part of 'widgets.dart';

class EmpleadoFormWidget extends StatefulWidget {
  EmpleadoFormWidget({Key? key, required this.onPressed}) : super(key: key);

  final void Function()? onPressed;

  @override
  _EmpleadoFormWidgetState createState() => _EmpleadoFormWidgetState(
        onPressed: this.onPressed,
      );
}

class _EmpleadoFormWidgetState extends State<EmpleadoFormWidget> {
  final _formKey = GlobalKey<FormState>();
  final primerNombreController = TextEditingController();
  final otrosNombresController = TextEditingController();
  final primerApellidoController = TextEditingController();
  final segundoApellidoController = TextEditingController();
  final numeroIdentificacionController = TextEditingController();
  final void Function()? onPressed;

  _EmpleadoFormWidgetState({required this.onPressed});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    BlocProvider.of<TipoDocumentoBloc>(context).init();
    BlocProvider.of<PaisBloc>(context).init();
    BlocProvider.of<AreaBloc>(context).init();
    return Form(
      key: _formKey,
      child: Container(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              CustomTextField(
                labelText: 'Primer nombre',
                controller: primerNombreController,
              ),
              SizedBox(
                height: 10.0,
              ),
              CustomTextField(
                labelText: 'Otros nombres',
                controller: otrosNombresController,
              ),
              SizedBox(
                height: 10.0,
              ),
              CustomTextField(
                labelText: 'Primer apellido',
                controller: primerApellidoController,
              ),
              SizedBox(
                height: 10.0,
              ),
              CustomTextField(
                labelText: 'Segundo apellido',
                controller: otrosNombresController,
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  BlocBuilder<TipoDocumentoBloc, TipoDocumentoState>(
                    builder: (context, state) {
                      return Container(
                        width: 50,
                        child: DropdownButtonFormField<int>(
                          items: state.tiposDocumento
                              .map((TipoIdentificacion value) {
                            return DropdownMenuItem<int>(
                              value: value.id,
                              child: new Text(value.codigo ?? ''),
                            );
                          }).toList(),
                          onChanged: (_) {
                            print(_);
                          },
                          value: state.tiposDocumento.length > 0
                              ? state.tiposDocumento[0].id
                              : 0,
                        ),
                      );
                    },
                  ),
                  Container(
                    width: size.width * 0.5,
                    child: CustomTextField(
                      labelText: 'Número identificación',
                      controller: otrosNombresController,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10.0),
              _dropDowns(),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    color: Colors.blueGrey[50],
                    child: Text('Cancelar'),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  MaterialButton(
                    onPressed: () {},
                    color: Colores.primario,
                    child: Text(
                      'Crear',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _dropDowns() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Area'),
            Container(
              child: BlocBuilder<AreaBloc, AreaState>(
                builder: (context, state) {
                  return DropdownButtonFormField<int>(
                    items: state.areas.map((Area value) {
                      return DropdownMenuItem<int>(
                        value: value.id,
                        child: new Text(
                          value.nombre ?? '',
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                    onChanged: (_) {},
                    value: state.areas.length > 0 ? state.areas[0].id : 0,
                  );
                },
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10.0,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('País'),
            Container(
              child: BlocBuilder<PaisBloc, PaisState>(
                builder: (context, state) {
                  return DropdownButtonFormField<int>(
                    items: state.paises.map((Pais value) {
                      return DropdownMenuItem<int>(
                        value: value.id,
                        child: new Text(
                          value.nombre ?? '',
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                    onChanged: (_) {},
                    value: state.paises.length > 0 ? state.paises[0].id : 0,
                  );
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
