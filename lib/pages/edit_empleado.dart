import 'package:flutter/material.dart';
import 'package:prueba_tecnica_app/models/empleado_model.dart';
import 'package:prueba_tecnica_app/widgets/widgets.dart';

class EditEmpleado extends StatelessWidget {
  EditEmpleado({Key? key, required this.empleado}) : super(key: key);

  final Empleado empleado;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        brightness: Brightness.dark,
        title: Text(
          empleado.getNombreCompleto(),
        ),
      ),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
        child: EmpleadoFormEdit(),
      ),
    );
  }
}
