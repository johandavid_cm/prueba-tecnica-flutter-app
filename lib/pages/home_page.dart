import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:prueba_tecnica_app/bloc/empleado/empleado_bloc.dart';
import 'package:prueba_tecnica_app/bloc/tipodocumento/tipodocumento_bloc.dart';
import 'package:prueba_tecnica_app/bloc/usuario/usuario_bloc.dart';
import 'package:prueba_tecnica_app/theme/colors.dart';
import 'package:prueba_tecnica_app/utils/dialogs.dart';
import 'package:prueba_tecnica_app/widgets/widgets.dart'
    show CustomDrawer, EmpleadoFormWidget, EmpleadoWidget;

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<EmpleadoBloc>(context).cargarEmpleados();
    return BlocBuilder<UsuarioBloc, UsuarioState>(
      builder: (context, state) {
        return MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (_) => TipoDocumentoBloc()..init(),
            )
          ],
          child: Scaffold(
            drawer: CustomDrawer(),
            appBar: AppBar(
              brightness: Brightness.dark,
              actions: [
                IconButton(
                  onPressed: () async {
                    // Eliminamos todo del auth
                    await FlutterSecureStorage().deleteAll();
                    BlocProvider.of<UsuarioBloc>(context).add(OnLogout());
                    Navigator.pushNamedAndRemoveUntil(
                        context, 'login', (route) => false);
                  },
                  icon: Icon(Icons.exit_to_app),
                ),
              ],
              title: Text(state.usuario?.email ?? ''),
              centerTitle: true,
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                BlocProvider.of<TipoDocumentoBloc>(context).init();
                Dialogs.showDialog(
                  context,
                  child: EmpleadoFormWidget(
                    onPressed: () {},
                  ),
                );
              },
              child: Icon(Icons.add),
              backgroundColor: Colores.orange,
            ),
            body: _ListViewEmpleados(),
          ),
        );
      },
    );
  }
}

class _ListViewEmpleados extends StatefulWidget {
  const _ListViewEmpleados({
    Key? key,
  }) : super(key: key);

  @override
  __ListViewEmpleadosState createState() => __ListViewEmpleadosState();
}

class __ListViewEmpleadosState extends State<_ListViewEmpleados> {
  final ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        // Si me encuentro al final del scroll controller
        // busco si hay mas empleados para recargar y verficio que el
        // máximo no haya sido alcanzado para evitar que se este recargando
        // constantemente
        final empleadoBloc = BlocProvider.of<EmpleadoBloc>(context);
        if (!empleadoBloc.state.maximoAlcanzado) {
          empleadoBloc.cargarEmpleados();
        }
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EmpleadoBloc, EmpleadoState>(
      builder: (context, state) {
        if (state.empleados.length == 0 && state.cargando) {
          return Center(
            child: CircularProgressIndicator(
              color: Colores.primario,
            ),
          );
        }
        if (state.empleados.length == 0 && !state.cargando) {
          return Center(
            child: Text(
              'La lista de empleados se encuentra vacia',
              style: Theme.of(context).textTheme.headline6,
            ),
          );
        }
        return ListView.builder(
          controller: _scrollController,
          physics: BouncingScrollPhysics(),
          itemBuilder: (context, i) {
            final empleado = state.empleados[i];
            if (state.cargando && i == state.empleados.length - 1) {
              return Column(
                children: [
                  EmpleadoWidget(empleado: empleado),
                  CircularProgressIndicator(
                    color: Colores.primario,
                  ),
                ],
              );
            } else {
              return EmpleadoWidget(
                empleado: empleado,
              );
            }
          },
          itemCount: state.empleados.length,
        );
      },
    );
  }
}
