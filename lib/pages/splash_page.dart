import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:prueba_tecnica_app/bloc/usuario/usuario_bloc.dart';
import 'package:prueba_tecnica_app/models/usuario_model.dart';
import 'package:prueba_tecnica_app/pages/home_page.dart';
import 'package:prueba_tecnica_app/pages/login_page.dart';
import 'package:prueba_tecnica_app/services/auth_service.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Primero verificamos si el usuario esta logueado y renovamos el token
      // En base a esto lo enviamos a una página u otra
      future: AuthService().renewToken(),
      builder: (context, AsyncSnapshot<Response<dynamic>?> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data!.statusMessage == 'Not Found') {
            return LoginPage();
          }
          if (!snapshot.data!.data["ok"]) {
            return LoginPage();
          }
          final loginResponse = UsuarioResponse.fromJson(snapshot.data!.data);
          BlocProvider.of<UsuarioBloc>(context)
              .add(OnLogin(loginResponse.usuario));
          FlutterSecureStorage()
              .write(key: 'token', value: loginResponse.token);
          return HomePage();
        }
        return Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          child: Image.asset(
            'assets/images/logo.jpg',
          ),
        );
      },
    );
  }
}
