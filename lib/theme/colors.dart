import 'package:flutter/material.dart' show Color;

class Colores {
  static Color primario = Color(0xFF1891c3);
  static Color scaffoldColor = Color(0xFFFFFFFF);
  static Color danger = Color(0xFF8f3030);
  static Color orange = Color(0xFFdc6221);
  static Color morado = Color(0xFF303a8f);
}
