import 'package:flutter/material.dart';
import 'package:prueba_tecnica_app/theme/colors.dart';

final themeData = ThemeData(
  primaryColor: Colores.primario,
  scaffoldBackgroundColor: Colores.scaffoldColor,
  buttonColor: Colores.primario,
  elevatedButtonTheme: ElevatedButtonThemeData(),
  buttonTheme: ButtonThemeData(
    buttonColor: Colores.primario,
  ),
  inputDecorationTheme: InputDecorationTheme(
    enabledBorder: InputBorder.none,
    border: InputBorder.none,
    focusedBorder: InputBorder.none,
    labelStyle: TextStyle(
      color: Colors.white,
      fontSize: 18.0,
      fontWeight: FontWeight.w500,
    ),
  ),
);
