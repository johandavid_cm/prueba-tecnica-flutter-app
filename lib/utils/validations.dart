String? validarCorreo(String? email) {
  String? mensaje;
  if (email?.isEmpty ?? true) {
    mensaje = 'El correo no debe de estar vacio';
  }
  if (!RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(email ?? '')) {
    mensaje = 'Digite un correo válido';
  }
  return mensaje;
}

String? validarPassword(String? password) {
  String? mensaje;
  if (password?.isEmpty ?? true) {
    mensaje = 'El password no debe de estar vacio';
  }
  return mensaje;
}
