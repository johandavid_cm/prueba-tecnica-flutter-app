import 'package:flutter/material.dart';
import 'package:prueba_tecnica_app/theme/colors.dart';

abstract class Dialogs {
  static showDialog(
    BuildContext context, {
    String title = 'Nuevo empleado',
    required Widget child,
    bool barrierDismissible = false,
  }) {
    final size = MediaQuery.of(context).size;
    showGeneralDialog(
      transitionBuilder: (context, a1, a2, widget) {
        return Transform.scale(
          scale: a1.value,
          child: Opacity(
            opacity: a1.value,
            child: AlertDialog(
              shape:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
              title: Text(title),
              content: IntrinsicHeight(
                child: Container(
                  child: child,
                  width: size.width * 0.8,
                ),
              ),
            ),
          ),
        );
      },
      transitionDuration: Duration(milliseconds: 200),
      barrierDismissible: barrierDismissible,
      barrierLabel: '',
      context: context,
      // ignore: missing_return
      pageBuilder: (context, animation1, animation2) => Container(),
    );
  }

  static showDialogConfirm(
    BuildContext context, {
    String title = 'Eliminar empleado',
    String description = '¿Esta seguro de eliminar este empleado?',
    bool barrierDismissible = false,
    required void Function() onpressed,
  }) {
    final size = MediaQuery.of(context).size;
    showGeneralDialog(
      transitionBuilder: (context, a1, a2, widget) {
        return Transform.scale(
          scale: a1.value,
          child: Opacity(
            opacity: a1.value,
            child: AlertDialog(
              shape:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
              title: Text(
                title,
                style: TextStyle(color: Colores.danger),
              ),
              content: IntrinsicHeight(
                child: Container(
                  width: size.width * 0.8,
                  child: Column(
                    children: [
                      Text(
                        description,
                        textAlign: TextAlign.center,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          MaterialButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text('Cancelar'),
                            color: Colors.white,
                          ),
                          MaterialButton(
                            onPressed: onpressed,
                            child: Text(
                              'Eliminar',
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colores.danger,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
      transitionDuration: Duration(milliseconds: 200),
      barrierDismissible: barrierDismissible,
      barrierLabel: '',
      context: context,
      // ignore: missing_return
      pageBuilder: (context, animation1, animation2) => Container(),
    );
  }
}
