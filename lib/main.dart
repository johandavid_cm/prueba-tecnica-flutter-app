import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_tecnica_app/bloc/area/area_bloc.dart';
import 'package:prueba_tecnica_app/bloc/empleado/empleado_bloc.dart';
import 'package:prueba_tecnica_app/bloc/pais/pais_bloc.dart';
import 'package:prueba_tecnica_app/bloc/tipodocumento/tipodocumento_bloc.dart';
import 'package:prueba_tecnica_app/bloc/usuario/usuario_bloc.dart';
import 'package:prueba_tecnica_app/pages/home_page.dart';
import 'package:prueba_tecnica_app/pages/login_page.dart';
import 'package:prueba_tecnica_app/pages/splash_page.dart';
import 'package:prueba_tecnica_app/theme/colors.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    // Manejar de estado utilizado flutter bloc
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => UsuarioBloc()),
        BlocProvider(create: (_) => EmpleadoBloc()),
        BlocProvider(create: (_) => TipoDocumentoBloc()),
        BlocProvider(create: (_) => AreaBloc()),
        BlocProvider(create: (_) => PaisBloc()),
      ],
      child: MaterialApp(
        title: 'Cidenet S.A.S.',
        debugShowCheckedModeBanner: false,
        home: SplashPage(),
        routes: {
          'home': (BuildContext context) => HomePage(),
          'login': (BuildContext context) => LoginPage(),
        },
        theme: ThemeData(
          primaryColor: Colores.primario,
          scaffoldBackgroundColor: Colores.scaffoldColor,
          buttonColor: Colores.primario,
          elevatedButtonTheme: ElevatedButtonThemeData(),
          buttonTheme: ButtonThemeData(
            buttonColor: Colores.primario,
          ),
          textTheme: TextTheme(
            headline6: TextStyle(
              color: Colores.primario,
              fontSize: 18,
            ),
          ),
          inputDecorationTheme: InputDecorationTheme(
            enabledBorder: InputBorder.none,
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            labelStyle: TextStyle(
              color: Colors.black54,
              fontSize: 18.0,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }
}
