class Area {
  Area({
    this.id,
    this.nombre,
  });

  int? id;
  String? nombre;

  factory Area.fromJson(Map<String, dynamic> json) => Area(
        id: json["id"],
        nombre: json["nombre"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
      };
}
