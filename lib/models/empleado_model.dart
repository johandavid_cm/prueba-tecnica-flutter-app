import 'package:prueba_tecnica_app/models/area_model.dart';
import 'package:prueba_tecnica_app/models/pais_model.dart';
import 'package:prueba_tecnica_app/models/tipo_identificacion_model.dart';

class Empleado {
  Empleado({
    this.id,
    this.primerApellido,
    this.segundoApellido,
    this.primerNombre,
    this.otrosNombres,
    this.numeroIdentificacion,
    this.email,
    this.estado,
    this.tipoIdentificacionId,
    this.areaId,
    this.paisId,
    this.fechaCreacion,
    this.fechaActualizacion,
    this.tipoIdentificacion,
    this.pais,
    this.area,
  });

  int? id;
  String? primerApellido;
  String? segundoApellido;
  String? primerNombre;
  String? otrosNombres;
  String? numeroIdentificacion;
  String? email;
  bool? estado;
  int? tipoIdentificacionId;
  int? areaId;
  int? paisId;
  DateTime? fechaCreacion;
  DateTime? fechaActualizacion;
  TipoIdentificacion? tipoIdentificacion;
  Pais? pais;
  Area? area;

  String getNombreCompleto() {
    return '${this.primerNombre ?? ''} ${this.primerApellido ?? ''}';
  }

  String getFirstLetters() {
    return '${this.primerNombre?.substring(0, 1) ?? ''}${this.primerApellido?.substring(0, 1) ?? ''}';
  }

  factory Empleado.fromJson(Map<String, dynamic> json) => Empleado(
        id: json["id"],
        primerApellido: json["primerApellido"],
        segundoApellido: json["segundoApellido"],
        primerNombre: json["primerNombre"],
        otrosNombres: json["otrosNombres"],
        numeroIdentificacion: json["numeroIdentificacion"],
        email: json["email"],
        estado: json["estado"],
        tipoIdentificacionId: json["tipoIdentificacionId"],
        areaId: json["areaId"],
        paisId: json["paisId"],
        fechaCreacion: DateTime.parse(json["fechaCreacion"]),
        fechaActualizacion: DateTime.parse(json["fechaActualizacion"]),
        tipoIdentificacion:
            TipoIdentificacion.fromJson(json["TipoIdentificacion"]),
        pais: Pais.fromJson(json["pais"]),
        area: Area.fromJson(json["area"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "primerApellido": primerApellido,
        "segundoApellido": segundoApellido,
        "primerNombre": primerNombre,
        "otrosNombres": otrosNombres,
        "numeroIdentificacion": numeroIdentificacion,
        "email": email,
        "estado": estado,
        "tipoIdentificacionId": tipoIdentificacionId,
        "areaId": areaId,
        "paisId": paisId,
        "fechaCreacion": fechaCreacion!.toIso8601String(),
        "fechaActualizacion": fechaActualizacion!.toIso8601String(),
        "TipoIdentificacion": tipoIdentificacion!.toJson(),
        "pais": pais!.toJson(),
        "area": area!.toJson(),
      };
}
