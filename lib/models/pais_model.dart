class Pais {
  Pais({
    this.id,
    this.codigo,
    this.nombre,
  });

  int? id;
  String? codigo;
  String? nombre;

  factory Pais.fromJson(Map<String, dynamic> json) => Pais(
        id: json["id"],
        codigo: json["codigo"],
        nombre: json["nombre"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "codigo": codigo,
        "nombre": nombre,
      };
}
