class TipoIdentificacion {
  TipoIdentificacion({
    this.id,
    this.codigo,
    this.nombre,
  });

  int? id;
  String? codigo;
  String? nombre;

  factory TipoIdentificacion.fromJson(Map<String, dynamic> json) =>
      TipoIdentificacion(
        id: json["id"],
        codigo: json["codigo"],
        nombre: json["nombre"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "codigo": codigo,
        "nombre": nombre,
      };
}
