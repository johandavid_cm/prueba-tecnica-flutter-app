import 'dart:convert';

import 'package:prueba_tecnica_app/models/empleado_model.dart';

EmpleadoResponse empleadoResponseFromJson(String str) =>
    EmpleadoResponse.fromJson(json.decode(str));

String empleadoResponseToJson(EmpleadoResponse data) =>
    json.encode(data.toJson());

class EmpleadoResponse {
  EmpleadoResponse({
    this.ok,
    this.msg,
    this.empleados,
  });

  bool? ok;
  String? msg;
  List<Empleado>? empleados;

  factory EmpleadoResponse.fromJson(Map<String, dynamic> json) =>
      EmpleadoResponse(
        ok: json["ok"],
        msg: json["msg"],
        empleados: List<Empleado>.from(
            json["empleados"].map((x) => Empleado.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "ok": ok,
        "msg": msg,
        "empleados": List<dynamic>.from(empleados ?? [].map((x) => x.toJson())),
      };
}
