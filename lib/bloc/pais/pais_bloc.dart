import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:prueba_tecnica_app/models/pais_model.dart';
import 'package:prueba_tecnica_app/services/pais_service.dart';

part 'pais_event.dart';
part 'pais_state.dart';

class PaisBloc extends Bloc<PaisEvent, PaisState> {
  PaisBloc() : super(PaisState());

  @override
  Stream<PaisState> mapEventToState(
    PaisEvent event,
  ) async* {
    if (event is OnCargando) {
      yield state.copyWith(cargando: event.cargando);
    } else if (event is OnCargarPaises) {
      yield state.copyWith(paises: event.paises);
    }
  }

  Future<void> init() async {
    this.add(OnCargando(cargando: true));
    final resp = await PaisService().getPaises();
    if (resp != null) {
      if (resp.data['ok']) {
        List<Pais> paises = List<Pais>.from(
          resp.data["paises"].map(
            (x) => Pais.fromJson(x),
          ),
        );
        this.add(OnCargarPaises(paises: paises));
      }
    }
    this.add(OnCargando(cargando: false));
  }
}
