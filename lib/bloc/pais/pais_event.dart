part of 'pais_bloc.dart';

@immutable
abstract class PaisEvent {}

class OnCargando extends PaisEvent {
  final bool cargando;

  OnCargando({required this.cargando});
}

class OnCargarPaises extends PaisEvent {
  final List<Pais> paises;

  OnCargarPaises({required this.paises});
}
