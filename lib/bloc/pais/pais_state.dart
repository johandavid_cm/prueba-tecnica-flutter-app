part of 'pais_bloc.dart';

@immutable
class PaisState {
  final bool cargando;
  final List<Pais> paises;

  PaisState({
    this.cargando = false,
    List<Pais>? paises,
  }) : this.paises = paises ?? [];

  PaisState copyWith({
    bool? cargando,
    List<Pais>? paises,
  }) =>
      PaisState(
        cargando: cargando ?? this.cargando,
        paises: paises ?? this.paises,
      );
}
