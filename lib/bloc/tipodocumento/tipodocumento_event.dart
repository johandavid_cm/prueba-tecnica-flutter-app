part of 'tipodocumento_bloc.dart';

@immutable
abstract class TipoDocumentoEvent {}

class OnCargando extends TipoDocumentoEvent {
  final bool cargando;

  OnCargando({required this.cargando});
}

class OnCargarTipo extends TipoDocumentoEvent {
  final List<TipoIdentificacion> tiposDocumento;

  OnCargarTipo({required this.tiposDocumento});
}
