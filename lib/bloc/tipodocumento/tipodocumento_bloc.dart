import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:prueba_tecnica_app/models/tipo_identificacion_model.dart';
import 'package:prueba_tecnica_app/services/tipo_documento_service.dart';

part 'tipodocumento_event.dart';
part 'tipodocumento_state.dart';

class TipoDocumentoBloc extends Bloc<TipoDocumentoEvent, TipoDocumentoState> {
  TipoDocumentoBloc() : super(TipoDocumentoState());

  @override
  Stream<TipoDocumentoState> mapEventToState(
    TipoDocumentoEvent event,
  ) async* {
    if (event is OnCargando) {
      yield state.copyWith(cargando: event.cargando);
    } else if (event is OnCargarTipo) {
      yield state.copyWith(tiposDocumento: event.tiposDocumento);
    }
  }

  Future<void> init() async {
    this.add(OnCargando(cargando: true));
    final resp = await TipoDocumentoService().getTiposDocumento();
    if (resp != null) {
      if (resp.data['ok']) {
        List<TipoIdentificacion> tiposDocumento = List<TipoIdentificacion>.from(
          resp.data["tiposDocumento"].map(
            (x) => TipoIdentificacion.fromJson(x),
          ),
        );
        this.add(OnCargarTipo(tiposDocumento: tiposDocumento));
      }
    }
    this.add(OnCargando(cargando: false));
  }
}
