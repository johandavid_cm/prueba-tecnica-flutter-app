part of 'tipodocumento_bloc.dart';

@immutable
class TipoDocumentoState {
  final bool cargando;
  final List<TipoIdentificacion> tiposDocumento;

  TipoDocumentoState({
    this.cargando = false,
    List<TipoIdentificacion>? tiposDocumento,
  }) : this.tiposDocumento = tiposDocumento ?? [];

  TipoDocumentoState copyWith({
    bool? cargando,
    List<TipoIdentificacion>? tiposDocumento,
  }) =>
      TipoDocumentoState(
        cargando: cargando ?? this.cargando,
        tiposDocumento: tiposDocumento ?? this.tiposDocumento,
      );
}
