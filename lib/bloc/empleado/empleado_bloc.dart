import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:prueba_tecnica_app/models/empleado_model.dart';
import 'package:prueba_tecnica_app/services/empleado_service.dart';

part 'empleado_event.dart';
part 'empleado_state.dart';

class EmpleadoBloc extends Bloc<EmpleadoEvent, EmpleadoState> {
  EmpleadoBloc() : super(EmpleadoState());

  @override
  Stream<EmpleadoState> mapEventToState(
    EmpleadoEvent event,
  ) async* {
    if (event is OnAddEmpleados) {
      final empleados = [...state.empleados, ...event.empleados];
      // A Continuación elimino valores repetidos para verificar la consistencia de los datos
      final ids = empleados.map((e) => e.id).toSet();
      empleados.retainWhere((x) => ids.remove(x.id));
      yield state.copyWith(empleados: empleados);
    } else if (event is OnCargando) {
      yield state.copyWith(cargando: event.cargando);
    } else if (event is OnPageChange) {
      yield state.copyWith(pagina: event.pagina);
    } else if (event is OnMaximoAlcanzado) {
      yield state.copyWith(maximoAlcanzado: true);
    } else if (event is OnEliminarEmpleado) {
      final empleados = state.empleados
          .where((empleado) => empleado.id != event.empleado.id)
          .toList();
      yield state.copyWith(empleados: empleados);
    }
  }

  Future<void> cargarEmpleados() async {
    this.add(OnCargando(cargando: true));
    final resp = await EmpleadoService().getEmpleados(
      page: state.pagina,
    );
    if (resp != null && resp.empleados != null) {
      final empleados = resp.empleados ?? [];
      if (empleados.length == 10) {
        this.add(
          OnPageChange(pagina: state.pagina + 1),
        );
      } else {
        this.add(OnMaximoAlcanzado(true));
      }
      this.add(
        OnAddEmpleados(empleados: empleados),
      );
    }
    this.add(OnCargando(cargando: false));
  }
}
