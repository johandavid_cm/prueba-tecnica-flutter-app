part of 'empleado_bloc.dart';

@immutable
class EmpleadoState {
  final List<Empleado> empleados;
  final bool cargando;
  final int pagina;
  final bool maximoAlcanzado;

  EmpleadoState({
    this.cargando = false,
    this.pagina = 1,
    List<Empleado>? empleados,
    this.maximoAlcanzado = false,
  }) : this.empleados = empleados ?? [];

  EmpleadoState copyWith({
    List<Empleado>? empleados,
    bool? cargando,
    int? pagina,
    bool? maximoAlcanzado,
  }) =>
      EmpleadoState(
        empleados: empleados ?? this.empleados,
        cargando: cargando ?? this.cargando,
        pagina: pagina ?? this.pagina,
        maximoAlcanzado: maximoAlcanzado ?? this.maximoAlcanzado,
      );
}
