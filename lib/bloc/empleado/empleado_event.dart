part of 'empleado_bloc.dart';

@immutable
abstract class EmpleadoEvent {}

class OnAddEmpleados extends EmpleadoEvent {
  final List<Empleado> empleados;

  OnAddEmpleados({required this.empleados});
}

class OnCargando extends EmpleadoEvent {
  final bool cargando;

  OnCargando({
    required this.cargando,
  });
}

class OnPageChange extends EmpleadoEvent {
  final int pagina;

  OnPageChange({required this.pagina});
}

class OnMaximoAlcanzado extends EmpleadoEvent {
  final bool maximoAlcanzado;

  OnMaximoAlcanzado(this.maximoAlcanzado);
}

class OnEliminarEmpleado extends EmpleadoEvent {
  final Empleado empleado;

  OnEliminarEmpleado(this.empleado);
}
