import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:prueba_tecnica_app/models/usuario_model.dart';

part 'usuario_event.dart';
part 'usuario_state.dart';

class UsuarioBloc extends Bloc<UsuarioEvent, UsuarioState> {
  UsuarioBloc() : super(UsuarioState());

  @override
  Stream<UsuarioState> mapEventToState(
    UsuarioEvent event,
  ) async* {
    if (event is OnLogin) {
      yield state.copyWith(usuario: event.usuario);
    } else if (event is OnLogout) {
      yield state.copyWith(usuario: null);
    }
  }
}
