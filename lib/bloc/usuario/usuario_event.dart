part of 'usuario_bloc.dart';

@immutable
abstract class UsuarioEvent {}

class OnLogin extends UsuarioEvent {
  final Usuario usuario;

  OnLogin(this.usuario);
}

class OnLogout extends UsuarioEvent {}
