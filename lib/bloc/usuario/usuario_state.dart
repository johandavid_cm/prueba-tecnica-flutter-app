part of 'usuario_bloc.dart';

@immutable
class UsuarioState {
  final Usuario? usuario;

  UsuarioState({this.usuario});

  UsuarioState copyWith({
    Usuario? usuario,
  }) =>
      UsuarioState(
        usuario: usuario ?? this.usuario,
      );
}
