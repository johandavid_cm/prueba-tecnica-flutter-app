part of 'area_bloc.dart';

@immutable
abstract class AreaEvent {}

class OnCargando extends AreaEvent {
  final bool cargando;

  OnCargando({required this.cargando});
}

class OnCargarAreas extends AreaEvent {
  final List<Area> areas;

  OnCargarAreas({required this.areas});
}

class OnSelectedAreaChange extends AreaEvent {
  final Area selectedArea;

  OnSelectedAreaChange(this.selectedArea);
}
