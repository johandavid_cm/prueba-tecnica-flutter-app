part of 'area_bloc.dart';

@immutable
class AreaState {
  final bool cargando;
  final List<Area> areas;
  final Area selectedArea;

  AreaState({
    Area? selectedArea,
    this.cargando = false,
    List<Area>? areas,
  })  : this.areas = areas ?? [],
        this.selectedArea = selectedArea ?? Area(id: 0, nombre: '');

  AreaState copyWith({
    bool? cargando,
    List<Area>? areas,
    Area? selectedArea,
  }) =>
      AreaState(
        cargando: cargando ?? this.cargando,
        areas: areas ?? this.areas,
        selectedArea: selectedArea ?? this.selectedArea,
      );
}
