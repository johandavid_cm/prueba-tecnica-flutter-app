import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:prueba_tecnica_app/models/area_model.dart';
import 'package:prueba_tecnica_app/services/area_service.dart';

part 'area_event.dart';
part 'area_state.dart';

class AreaBloc extends Bloc<AreaEvent, AreaState> {
  AreaBloc() : super(AreaState());

  @override
  Stream<AreaState> mapEventToState(
    AreaEvent event,
  ) async* {
    if (event is OnCargando) {
      yield state.copyWith(cargando: event.cargando);
    } else if (event is OnCargarAreas) {
      yield state.copyWith(areas: event.areas);
    }
  }

  Future<void> init() async {
    this.add(OnCargando(cargando: true));
    final resp = await AreaService().getAreas();
    if (resp != null) {
      if (resp.data['ok']) {
        List<Area> areas = List<Area>.from(
          resp.data["areas"].map(
            (x) => Area.fromJson(x),
          ),
        );
        this.add(OnCargarAreas(areas: areas));
      }
    }
    this.add(OnCargando(cargando: false));
  }
}
